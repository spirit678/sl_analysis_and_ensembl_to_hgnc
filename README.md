# SL_analysis_and_ensembl_to_hgnc

Two tasks:
 
## convert ensembl ID into HGNC gene symbols

 - Ensembl_to_hgnc.html - render of notebook
 - Ensembl_to_hgnc.Rmd - code itself
 - assigned_symbols.csv - results for all transcripts and genes in the test_ids_input.tsv
 
 Reproducible env:
 
 ```bash
 docker run -d -v $(pwd):/home/rstudio -p 8787:8787 -e PASSWORD=yourpasswordhere rocker/rstudio:4.1
 ```
 
 
## Analysis of synthetic lethality of STAG1 and STAG2

I've made using two different approaches:


### DiscoverSL

First aproach is grounded on [DiscoverSL](https://academic.oup.com/bioinformatics/article/35/4/701/5061158)

you need to download [package from git](https://github.com/shaoli86/DiscoverSL/releases/tag/V1.0)

as well as [cgdsr_1.3.0.tar.gz from cran](https://cran.r-project.org/src/contrib/Archive/cgdsr/cgdsr_1.3.0.tar.gz)

Environment:

 ```bash
 docker run -d -v $(pwd):/home/rstudio -p 8787:8787 -e PASSWORD=yourpasswordhere rocker/rstudio:4.1
 ```
 
 Dont forget to install system dependencies:
 
 Go into container
  ```bash
 docker exec -it -u root {your_container_id} bash
 ```
 And install
 
 ```bash
 apt update
 apt install fftw3 libfftw3-dev
 ```
#### Results

 - Output files - DiscoverSL_output
 - Notebook - DiscoverSL.Rmd
 - Rendered version - DiscoverSL.html

### Paralog_SL_prediction_analysis

Environment

```bash
docker run --rm -v $(pwd):/home/jovyan/work -e GRANT_SUDO=yes -e JUPYTER_ENABLE_LAB=yes -p 8888:8888 jupyter/datascience-notebook:latest
```

#### Results

 - Notebook - Paralog_SL_prediction_analysis.ipynb
 - Rendered version - Paralog_SL_prediction_analysis.html


## Project structure structure
```
├── [    6061015]  assigned_symbols.csv
├── [     738053]  DiscoverSL.html
├── [       4096]  DiscoverSL_output
│   ├── [       1092]  ALL.csv
│   ├── [        298]  BRCA.csv
│   ├── [        299]  HNSC.csv
│   ├── [        298]  KIRC.csv
│   ├── [        294]  LIHC.csv
│   ├── [        296]  LUSC.csv
│   ├── [        298]  SKCM.csv
│   ├── [        297]  STAD.csv
│   └── [        294]  UCEC.csv
├── [      10272]  DiscoverSL.Rmd
├── [     629033]  Ensembl_to_hgnc.html
├── [       3406]  Ensembl_to_hgnc.Rmd
├── [    9401714]  mart_export.txt
├── [     879452]  Paralog_SL_prediction_analysis.html
├── [     324076]  Paralog_SL_prediction_analysis.ipynb
└── [       6378]  README.md

